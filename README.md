## Сборка и запуск
```
git clone https://ilyamoskalev@bitbucket.org/ilyamoskalev/test_tinkoff.git
cd test_tinkoff/
mvn package
java -jar target/test-1.0-SNAPSHOT.jar
```

## Пример запроса
```
curl -X POST \
  http://localhost:8081/trains/ \
  -H 'Content-Type: application/json' \
  -d '{
	"state": "010010010001111001"
}'

curl -X GET http://localhost:8081/trains/1
```
