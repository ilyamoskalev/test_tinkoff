package application.it;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
public class TrainsITest {
    @Autowired
    private MockMvc mock;

    @Test
    public void testSaveStateNullState() throws Exception {
        mock.perform(post("/trains")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void testSaveStateInvalidState() throws Exception {
        mock.perform(post("/trains")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\"state\":\"gwcwcgvegcve722472hjbdwc\""))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void testSaveStateEmptyState() throws Exception {
        mock.perform(post("/trains")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\"state\":\"\""))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void testSaveStateAndGetLength() throws Exception {
        mock.perform(post("/trains")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\"state\":\"010100001110000111\"}"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("id").isNumber());

        mock.perform(get("/trains/1"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("length").value("18"));
    }

    @Test
    public void testGetStateNotFound() throws Exception {
        mock.perform(get("/trains/124724"))
            .andExpect(status().isNotFound());
    }
}
