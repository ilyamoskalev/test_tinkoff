package application.ut;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import application.dao.TrainDAO;
import application.services.TrainService;

public class TrainServiceTest {
    @Mock
    private TrainDAO trainDAO;

    private TrainService trainService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(trainDAO.saveState(Mockito.any())).thenReturn(1L);

        trainService = new TrainService(trainDAO);
    }

    @Test
    public void testSaveStateEmptyState() {
        Assert.assertNull(trainService.saveState(""));

        Mockito.verify(trainDAO, Mockito.never()).saveState(Mockito.any());
    }

    @Test
    public void testSaveStateNullState() {
        Assert.assertNull(trainService.saveState(null));

        Mockito.verify(trainDAO, Mockito.never()).saveState(Mockito.any());
    }

    @Test
    public void testSaveStateInvalidState() {
        Assert.assertNull(trainService.saveState("245678ejhfcbe"));

        Mockito.verify(trainDAO, Mockito.never()).saveState(Mockito.any());
    }

    @Test
    public void testSaveState() {
        Assert.assertEquals(Long.valueOf(1L), trainService.saveState("0101010101011110001111001010101"));

        Mockito.verify(trainDAO, Mockito.only()).saveState(Mockito.any());
    }

    @Test
    public void testGetLengthNoState() {
        Mockito.when(trainDAO.getStateById(2L)).thenReturn(null);

        Assert.assertFalse(trainService.getLengthById(2L).isPresent());

        Mockito.verify(trainDAO, Mockito.only()).getStateById(Mockito.eq(2L));
    }

    @Test
    public void testGetLength() {
        Boolean[] state = {true, false, false, true, true, true, true};
        Mockito.when(trainDAO.getStateById(1L)).thenReturn(state);

        Assert.assertEquals(Integer.valueOf(7), trainService.getLengthById(1L).get());

        Mockito.verify(trainDAO, Mockito.only()).getStateById(Mockito.eq(1L));
    }

    @Test
    public void testGetLengthOneCarriageLightTurnedON() {
        Boolean[] state = {true};
        Mockito.when(trainDAO.getStateById(3L)).thenReturn(state);

        Assert.assertEquals(Integer.valueOf(1), trainService.getLengthById(3L).get());

        Mockito.verify(trainDAO, Mockito.only()).getStateById(Mockito.eq(3L));
    }

    @Test
    public void testGetLengthOneCarriageLightTurnedOff() {
        Boolean[] state = {false};
        Mockito.when(trainDAO.getStateById(4L)).thenReturn(state);

        Assert.assertEquals(Integer.valueOf(1), trainService.getLengthById(4L).get());

        Mockito.verify(trainDAO, Mockito.only()).getStateById(Mockito.eq(4L));
    }
}
