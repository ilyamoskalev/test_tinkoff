package application.services;

import java.util.Arrays;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.springframework.stereotype.Service;

import application.dao.TrainDAO;

@Service
public class TrainService {
    private static final Pattern STATE_PATTERN = Pattern.compile("^[01]+$");
    private static final int STATE_MAX_SIZE = 50000;

    private final LoadingCache<Long, Optional<Integer>> lengthsCache = CacheBuilder.newBuilder()
        .maximumSize(1000)
        .expireAfterWrite(10, TimeUnit.MINUTES)
        .build(new CacheLoader<Long, Optional<Integer>>() {
            public Optional<Integer> load(Long id) {
                return loadCache(id);
            }
        });

    private final Random random = new Random();
    private final TrainDAO trainDAO;

    public TrainService(TrainDAO trainDAO) {
        this.trainDAO = trainDAO;
    }

    public Long saveState(String state) {
        if (state == null || state.length() > STATE_MAX_SIZE || !STATE_PATTERN.matcher(state).matches()) {
            return null;
        }

        Boolean[] booleans = Arrays.stream(state.split(""))
            .map(s -> s.equals("1"))
            .toArray(Boolean[]::new);

        return trainDAO.saveState(booleans);
    }

    public Optional<Integer> getLengthById(Long id) {
        return lengthsCache.getUnchecked(id);
    }

    private Optional<Integer> loadCache(Long id) {
        Boolean[] state = trainDAO.getStateById(id);
        if (state == null) {
            return Optional.empty();
        }

        return Optional.of(countLength(buildTrain(state)));
    }

    private int countLength(Carriage carriage) {
        if (!carriage.isTurnedOn()) {
            carriage.turnOnLamp();
        }

        while (true) {
            carriage = carriage.next();
            int length = 1;
            while (!carriage.isTurnedOn()) {
                carriage = carriage.next();
                ++length;
            }

            carriage.turnOffLamp();
            for (int i = 0; i < length; ++i) {
                carriage = carriage.previous();
            }

            if (!carriage.isTurnedOn()) {
                return length;
            }
        }
    }

    private Carriage buildTrain(Boolean[] state) {
        Carriage[] carriages = Arrays.stream(state)
            .map(Carriage::new)
            .toArray(Carriage[]::new);

        int length = carriages.length;
        if (length == 1) {
            carriages[0].setNext(carriages[0]);
            carriages[0].setPrevious(carriages[0]);
        } else {
            for (int i = 1; i < length - 1; ++i) {
                carriages[i].setPrevious(carriages[i - 1]);
                carriages[i].setNext(carriages[i + 1]);
            }

            carriages[0].setNext(carriages[1]);
            carriages[0].setPrevious(carriages[length - 1]);

            carriages[length - 1].setNext(carriages[0]);
            carriages[length - 1].setPrevious(carriages[length - 2]);
        }

        return carriages[random.nextInt(length)];
    }

    static class Carriage {
        private Carriage next;
        private Carriage previous;
        private boolean turnedOn;

        Carriage(boolean turnedOn) {
            this.turnedOn = turnedOn;
        }

        Carriage next() {
            return next;
        }

        Carriage previous() {
            return previous;
        }

        boolean isTurnedOn() {
            return turnedOn;
        }

        void turnOffLamp() {
            this.turnedOn = false;
        }

        void turnOnLamp() {
            this.turnedOn = true;
        }

        void setNext(Carriage next) {
            this.next = next;
        }

        void setPrevious(Carriage previous) {
            this.previous = previous;
        }
    }
}
