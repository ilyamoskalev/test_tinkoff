package application.response;

public class LengthResponse {
    private int length;

    public LengthResponse(int length) {
        this.length = length;
    }

    public int getLength() {
        return length;
    }
}
