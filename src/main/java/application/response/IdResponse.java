package application.response;

public class IdResponse {
    private long id;

    public IdResponse(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
}
