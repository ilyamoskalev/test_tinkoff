package application.controllers;

import java.util.Optional;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import application.requests.SaveStateRequest;
import application.response.IdResponse;
import application.response.LengthResponse;
import application.response.MessageResponse;
import application.services.TrainService;

@RestController
@RequestMapping(path = "/trains")
public class TrainController {
    private static final String INVALID_STATE = "Invalid state.";

    private final TrainService trainService;

    public TrainController(TrainService trainService) {
        this.trainService = trainService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity saveState(@RequestBody SaveStateRequest request) {
        Long id = trainService.saveState(request.getState());
        if (id == null) {
            return ResponseEntity.badRequest().body(new MessageResponse(INVALID_STATE));
        }

        return ResponseEntity.ok(new IdResponse(id));
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getLength(@PathVariable Long id) {
        Optional<Integer> length = trainService.getLengthById(id);
        if (!length.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(new LengthResponse(length.get()));
    }
}
