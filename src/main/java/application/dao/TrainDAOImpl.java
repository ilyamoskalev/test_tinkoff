package application.dao;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class TrainDAOImpl implements TrainDAO {
    private static final RowMapper<Boolean[]> ROW_MAPPER = (res, num) -> (Boolean[]) res.getArray("state").getArray();

    private final JdbcTemplate jdbcTemplate;

    public TrainDAOImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public long saveState(Boolean[] state) {
        String query = "INSERT INTO trains(state) VALUES (?) ";
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setArray(1, connection.createArrayOf("boolean", state));

            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

    @Override
    public Boolean[] getStateById(Long id) {
        String query = "SELECT state FROM trains WHERE id = ?";
        List<Boolean[]> state = jdbcTemplate.query(query, ROW_MAPPER, id);
        if (state.isEmpty()) {
            return null;
        }

        return state.get(0);
    }
}
