package application.dao;

public interface TrainDAO {
    long saveState(Boolean[] state);

    Boolean[] getStateById(Long id);
}
