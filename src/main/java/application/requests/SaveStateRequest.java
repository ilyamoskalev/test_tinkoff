package application.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SaveStateRequest {
    private final String state;

    @JsonCreator
    public SaveStateRequest(@JsonProperty("state") String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }
}
